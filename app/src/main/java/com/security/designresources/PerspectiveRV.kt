package com.security.designresources

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.core.view.marginBottom
import androidx.core.view.marginTop
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_card.view.*

class PerspectiveRV @JvmOverloads constructor(
    ctx:Context,
    attributeSet: AttributeSet? = null,
    defStyle:Int = 0
) : RecyclerView(ctx,attributeSet,defStyle){

    init {
        layoutManager = LinearLayoutManager(ctx,LinearLayoutManager.VERTICAL,false)
        adapter = com.security.designresources.Adapter()
        setHasFixedSize(true)
    }

    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)
        val lm = layoutManager as LinearLayoutManager
        val firstVisible = lm.findFirstVisibleItemPosition()
        var scaleFor4LastItems = 0f
        var rotationFor4LastItems = 0f
        var allItemsCount = adapter!!.itemCount

        children.forEachIndexed { index, view ->
            view.scaleX = 1f
            view.scaleY = 1f
            view.rotationX = 0f

            val childBoxHeight = view.marginBottom + view.height + view.marginTop
            val childTransitionState = ( (view.bottom + view.marginBottom) - index*childBoxHeight).toFloat()/childBoxHeight

            val startScale = getScaleForState(index)
            val endScale = getScaleForState(index+1)
            if(index + firstVisible == allItemsCount - 4){
                scaleFor4LastItems = childTransitionState*(endScale - startScale) + startScale
            }
            if(index + firstVisible > allItemsCount - 4){
                view.scaleX = scaleFor4LastItems
                view.scaleY = scaleFor4LastItems
            }else{
                view.scaleX = childTransitionState*(endScale - startScale) + startScale
                view.scaleY = childTransitionState*(endScale - startScale) + startScale
            }

            val startRotation = getRotationForState(index)
            val endRotation = getRotationForState(index + 1)
            if(index + firstVisible == allItemsCount - 4){
                rotationFor4LastItems = childTransitionState * (endRotation - startRotation) + startRotation
            }
            if(index + firstVisible > allItemsCount - 4){
                view.rotationX = rotationFor4LastItems
            }else{
                view.rotationX = childTransitionState * (endRotation - startRotation) + startRotation
            }
        }
    }

    private fun getScaleForState(state:Int):Float{
        return when(state){
            0 -> 1f
            1 -> 1f
            2 -> 0.9f
            3 -> 0.8f
            4 -> 0.7f
            else -> 0.65f
        }
    }

    private fun getRotationForState(state:Int):Float{
        return when(state){
            0 -> -10f
            1 -> 0f
            else -> 20f
        }
    }
}

class Adapter : RecyclerView.Adapter<Adapter.VH>(){

    data class Data(val title:String,val color:Int,val desc:String,val button:String)
    lateinit var layoutInflater: LayoutInflater
    var recyclerView:RecyclerView? = null
    val desc = "What's the difference between this, and simple custom view for each row in recyclerView that has height set to wrap content."
    val button = "Press Meee"
    val data = arrayOf(
        Data("Figma", Color.BLACK,desc,button),
        Data("Dribbble",Color.RED,desc, button),
        Data("Facebook",Color.BLUE,desc,button),
        Data("Sketch",Color.YELLOW,desc,button),
        Data("Invision",Color.RED,desc,button),
        Data("Unsplash",Color.BLACK,desc,button),
        Data("Twitter",Color.BLUE,desc,button),
        Data("Behance",Color.GREEN,desc,button)
    )

    class VH(itemView: View):RecyclerView.ViewHolder(itemView)

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        layoutInflater = LayoutInflater.from(recyclerView.context)
        this.recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(layoutInflater.inflate(R.layout.layout_card,parent,false).apply {
            layoutParams.height = recyclerView!!.height/4 - (marginBottom + marginTop)
            requestLayout()
        })
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.apply {
            txt_title.text = data[position].title
            txt_desc.text = data[position].desc
            btn.text = data[position].button
            root.setCardBackgroundColor(data[position].color)
        }
    }
}